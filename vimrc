"set nocompatible              " be iMproved, required

" Enable type file detection. Vim will be able to try to detect the type of file in use.
filetype on

" Enable plugins and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on

" Syntax highlighting
syntax on

" Color scheme
"colorscheme simpleblack_jake
"colorscheme darkblue
"colorscheme desert
"colorscheme default
"colorscheme elflord -- best colorscheme after gruvbox
"colorscheme elflord

" set colorscheme to gruvbox
let g:gruvbox_italic=1
let g:gruvbox_underline=1
let g:gruvbox_undercurl=1
let g:gruvbox_bold=1
let g:gruvbox_number_column='bg1'
let g:gruvbox_sign_column='bg0'
let g:gruvbox_color_column='bg1'
let g:gruvbox_vert_split='bg0'
let g:gruvbox_transparent_bg=0
let g:gruvbox_termcolors=256
"let g:gruvbox_termcolors=16
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light='hard'
let g:gruvbox_italicize_comments=1
let g:gruvbox_italicize_strings=0
let g:gruvbox_invert_selection=0
let g:gruvbox_invert_signs=0
let g:gruvbox_invert_indent_guides=0
let g:gruvbox_invert_tabline=0
let g:gruvbox_improved_strings=0
let g:gruvbox_improved_warnings=1
"let g:gruvbox_guisp_fallback='NONE'
let g:gruvbox_guisp_fallback='bg'
"let g:gruvbox_guisp_fallback='fg'
function! Highlight() abort
	"hi Conceal ctermfg=239 guifg=#504945
	"hi CocSearch ctermfg=12 guifg=#18A3FF
	"hi CocErrorHighlight ctermfg=red  guifg=#c4384b gui=undercurl term=undercurl
	"hi CocWarningHighlight ctermfg=yellow guifg=#c4ab39 gui=undercurl term=undercurl
	"hi CocWarningLine ctermfg=yellow guibg=#c4ab39 gui=undercurl term=undercurl
	"hi CocInfoLine ctermfg=yellow guibg=#c4ab39 gui=undercurl term=undercurl
	"hi CocInfoLine ctermbg=DarkYellow
	"hi CocInfoHighlight ctermbg=DarkYellow term=bold
	" https://github.com/gruvbox-community/gruvbox/pull/87
	hi! link haskelltype gruvboxyellow

	hi! link haskellidentifier gruvboxgreen

	hi! link haskellseparator gruvboxfg3
	hi! link haskelldelimiter gruvboxfg3
	hi! link haskelloperators gruvboxblue
	hi! link haskellbacktick haskelloperators

	hi! link haskellstatement gruvboxorange

	hi! link haskellconditional gruvboxred
	hi! link haskellkeyword gruvboxred
	hi! link haskelllet gruvboxred
	hi! link haskellwhere gruvboxred

	hi! link haskelldefault gruvboxaqua
	hi! link haskellbottom gruvboxaqua
	hi! link haskellblockkeywords gruvboxaqua
	hi! link haskellimportkeywords gruvboxaqua
	hi! link haskelldeclkeyword gruvboxaqua
	hi! link haskelldecl gruvboxaqua
	hi! link haskellderiving gruvboxaqua
	hi! link haskellassoctype gruvboxaqua

	hi! link haskellnumber gruvboxpurple
	hi! link haskellpragma gruvboxpurple

	hi! link haskellstring gruvboxgreen
	hi! link haskellchar gruvboxgreen
endfunction
autocmd vimenter * ++nested colorscheme gruvbox
autocmd ColorScheme * call Highlight()
set background=dark    " Setting dark mode
"set background=light   " Setting light mode
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

"" Set color scheme for vimdiff
"if &diff
"	colorscheme desert
"endif

" Add '$' at end of each line
"set list

" Set the leader key
let mapleader=" "

" number of colors
set t_Co=256


" Line numbering
set number
" Keep cursor at least 8 lines away from top or bottom
set scrolloff=8

" Show command
set showcmd

" Cursor line highlight
set cursorline

" Autocomplete commands
set wildmenu

" Enable folding of code blocks
set foldenable
" Set fold level of blocks folded by default
set foldlevelstart=10
" Fold Hotkeys
" Un-/Fold current
nnoremap <leader>w za
" Unfold everything
nnoremap <leader>e zR
" Fold everything
nnoremap <leader>q zM
" Method used for folding (see :help foldmethod)
set foldmethod=syntax
"set foldmethod=indent
"set foldmethod=marker


" Don't wrap lines
set nowrap
" technically not needed (see :help showbreak)
"set showbreak=+++ 

" Maximum width of text that is being inserted.
set textwidth=0

" Show matching brackets {[()]}
set showmatch

" Use a visual bell instead of beeping
set visualbell

" Highlight search results
set hlsearch
" start highlighting as soon as you start typing for search
set incsearch
" Ignore case of normal letters in searches
set ignorecase
" Ignore the 'ignorecase' option if the search pattern contains upper case
" characters.
set smartcase
 
" Copy indent from current line when starting a new line
"set autoindent

"set cindent
"set shiftwidth=4

"  Do smart autoindenting when starting a new line.
set smartindent
"set smarttab
"set softtabstop=4
set shiftwidth=8
set tabstop=8

" Show line and column number in the bottom right
set ruler
 
" Add line at 80 characters
highlight ColorColumn ctermbg=235 guibg=#2c2d27
"let &colorcolumn=join(range(81,999),",")
set colorcolumn=80


set undolevels=1000
set backspace=indent,eol,start




map <leader>s :source ~/.vim/vimrc<CR>
function CallMake()
	:set noshowcmd
	:w
	:set noshowmode
	":set noruler
	:set lazyredraw
	:set laststatus=0
	:!sh -c "~/.vim/makehelper.sh ; true"
	:set nolazyredraw
	:set laststatus=2
	":set ruler
	:set showmode
	:set showcmd
	:redraw
endfunction

"map <leader>m :!make<CR>
map <leader>m :call CallMake()<CR>

" Flying colorful triangles.
function! Triangles(...)
	Untitled
	colorscheme gruvbox
endfunction
map <leader>t :call Triangles()<CR>

"" Jump to next <++> -- part of latex-suite
"imap <C-v> <Plug>IMAP_JumpForward
"imap <C-x> <Plug>IMAP_JumpForward

" (Re)edit current file.
nnoremap <Leader><Leader> :e#<CR>


" Fullscreen current window using Ctrl+ww
nnoremap <silent> <C-w>w :ZoomWin<CR>

" Set Tabwidth inside YAML Files
autocmd FileType yaml setlocal shiftwidth=2 softtabstop=2


" --- Plugin Stuff
" Add Plugins by adding them below and then running ':so %' and
" ':PlugInstall'.
call plug#begin()
" This is where we will add plugins to install
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" Plug 'airblade/vim-gitgutter'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'tpope/vim-fugitive'

" Plug 'ctrlpvim/ctrlp.vim'
Plug '~/.vim/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'wikitopian/hardmode'

Plug 'vim-scripts/ZoomWin'

Plug 'MeF0504/untitled.vim'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'yaegassy/coc-pylsp', {'do': 'yarn install --frozen-lockfile'}

Plug 'morhetz/gruvbox'

Plug 'neovimhaskell/haskell-vim'

call plug#end()

" --- FZF (Fuzzy Finder)
command! -bang -nargs=? -complete=dir JFiles
    \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({'source': '~/.vim/lsgit.sh', 'options': ['--info=inline']}), <bang>0)
"nmap <C-P> :call fzf#run({'source': '~/.vim/lsgit.sh', 'sink': 'e'})<CR>
"nmap <C-P> :FZF<CR>
nmap <C-P> :JFiles<CR>


" --- NERDTree stuff
function! NerdTreeShouldBeOpen(...)
	"echo &columns
	if (( winnr("$") >= 2 && winwidth(0) <= 127 ) || &columns <= (127 + 32))
		return 0
	else
		return 1
	endif
endfunction

function! NerdTreeCloseIfTooSmall(...)
	if ( ! NerdTreeShouldBeOpen() )
		NERDTreeClose
	endif
endfunction
"if (&diff && winwidth('%') >= 240) || (!&diff && winwidth('%') >= 120) " Minimum width

" Disable NerdTree by default as it interfers with some coc.nvim features
"if ( NerdTreeShouldBeOpen() )
"	autocmd VimEnter * NERDTree
"	autocmd VimEnter * wincmd p
"endif

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd bufenter * call NerdTreeCloseIfTooSmall()
autocmd TermChanged * call NerdTreeCloseIfTooSmall()
autocmd VimResized * call NerdTreeCloseIfTooSmall()
autocmd WinNew * call NerdTreeCloseIfTooSmall()
autocmd VimEnter * call NerdTreeCloseIfTooSmall()

map <C-n> :NERDTreeToggle<CR>

"let g:NERDTreeIndicatorMapCustom = {
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }


"nmap ]h <Plug>(GitGutterNextHunk) "same as default
"nmap [h <Plug>(GitGutterPrevHunk) "same as default
"
"nmap ghs <Plug>(GitGutterStageHunk)
"nmap ghu <Plug>(GitGutterUndoHunk)
"
"let g:gitgutter_sign_added = '✚'
"let g:gitgutter_sign_modified = '✹'
"let g:gitgutter_sign_removed = '-'
"let g:gitgutter_sign_removed_first_line = '-'
"let g:gitgutter_sign_modified_removed = '-'


" -- airline stuff (status bar)
let g:airline_powerline_fonts = 1

" -- enable hard mode
"autocmd VimEnter,BufNewFile,BufReadPost * silent! call HardMode()
"let g:HardMode_level = 'wannabe'
"let g:HardMode_hardmodeMsg = 'Don''t use this!'
"autocmd VimEnter,BufNewFile,BufReadPost * silent! call HardMode()


" --- LSP & coc.nvim stuff (https://github.com/neoclide/coc.nvim#example-vim-configuration)
let g:coc_node_path = "~/.vim/node/bin/node"

" Always show the signcolumn to prevent bugs.
set signcolumn=yes

" May need for Vim (not Neovim) since coc.nvim calculates byte offset by count
" utf-8 byte sequence
set encoding=utf-8

" Some language servers have issues with backup files, see coc.nvim#649
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4s) leads to noticeable
" delays and poor user experience with coc.nvim
set updatetime=300

" Use tab for trigger completion with characters ahead and navigate
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"


" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"


function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif


" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <silent> üg <Plug>(coc-diagnostic-prev)
nmap <silent> +g <Plug>(coc-diagnostic-next)
nmap <silent> <leader>p <Plug>(coc-diagnostic-prev)
nmap <silent> <leader>n <Plug>(coc-diagnostic-next)


" GoTo code navigation
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)


" Use K to show documentation in preview window
nnoremap <silent> K :call ShowDocumentation()<CR>
nnoremap <silent> <leader>k :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor
autocmd CursorHold * silent call CocActionAsync('highlight')


" Symbol renaming
nmap <leader>rn <Plug>(coc-rename)
nmap <F2> <Plug>(coc-rename)

" Formatting selected code
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)


augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s)
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end


" Applying code actions to the selected code block
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying code actions at the cursor position
nmap <leader>ac  <Plug>(coc-codeaction-cursor)
" Remap keys for apply code actions affect whole buffer
nmap <leader>as  <Plug>(coc-codeaction-source)
" Apply the most preferred quickfix action to fix diagnostic on the current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Remap keys for applying refactor code actions
nmap <silent> <leader>re <Plug>(coc-codeaction-refactor)
xmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)
nmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)

" Run the Code Lens action on the current line
nmap <leader>cl  <Plug>(coc-codelens-action)

"" Map function and class text objects -- Doesn't work or I don't understand it. ~ Jake
"" NOTE: Requires 'textDocument.documentSymbol' support from the language server
"xmap if <Plug>(coc-funcobj-i)
"omap if <Plug>(coc-funcobj-i)
"xmap af <Plug>(coc-funcobj-a)
"omap af <Plug>(coc-funcobj-a)
"xmap ic <Plug>(coc-classobj-i)
"omap ic <Plug>(coc-classobj-i)
"xmap ac <Plug>(coc-classobj-a)
"omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> to scroll float windows/popups
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  nnoremap <silent><nowait><expr> <C-PageDown> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-PageDown>"
  nnoremap <silent><nowait><expr> <C-PageUp> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-PageUp>"
  inoremap <silent><nowait><expr> <C-PageDown> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-PageUp> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-PageDown> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-PageDown>"
  vnoremap <silent><nowait><expr> <C-PageUp> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-PageUp>"
endif


" Use CTRL-S for selections ranges
" Requires 'textDocument/selectionRange' support of language server
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')
" Hotkey for organizing imports
map <leader>o :OR<CR>

" Add (Neo)Vim's native statusline support
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"" Mappings for CoCList
"" Show all diagnostics
"nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
"" Manage extensions
"nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
"" Show commands
"nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
"" Find symbol of current document
"nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
"" Search workspace symbols
"nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
"" Do default action for next item
"nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
"" Do default action for previous item
"nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
"" Resume latest coc list
"nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" -- configure haskell-vim
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords
