# Jake's Vim Config

It is recommended to use this vim config in conjunction to [Jake's Bash Config](https://gitlab.gwdg.de/jakes-setup/bash-config).

## How-To-Install

1. Make a backup of your current `.vim` directory.
1. Delete your current `.vim` directory.
1. Run `[ ! -d ~/.vim ] && git clone https://gitlab.gwdg.de/jakes-setup/vim-config.git ~/.vim && ~/.vim/update.sh`.

   Use `[ ! -d ~/.vim ] && git clone git@gitlab.gwdg.de:jakes-setup/vim-config.git ~/.vim && ~/.vim/update.sh` if you are Jake.
1. Run `sudo apt install python3.10-venv`.

Note: This will also install [fzf](https://github.com/junegunn/fzf).

## How-To-Update

`~/.vim/update.sh`

## Cheatsheet

Leader: `<space>`

### Normal mode
- `<leader>q`: Fold everything
- `<leader>w`: Un-/Fold current
- `<leader>e`: Unfold everything
- `<leader>s`: Reload vimrc file
- `<leader>m`: Build current project. (Using make or ant.)
- `<leader>t`: Flying colorful triangles
- `<leader>e`: (Re-)Edit current file. (Useful to go back to original file after switching.)
- `<leader>rn` or `<F2>`: Rename current variable.
- `<leader>f`: Reformat current line.
- `<leader>n` or `]g` or `+g`: Jump to next coc diagnostic.
- `<leader>p` or `[g` or `üg`: Jump to prev coc diagnostic.
- `<leader>a`: Apply code action to the selected block.
- `<leader>ac`: Apply code action at the current cursor location.
- `<leader>qf`: Apply first code action on the current line. (Quickfix.)
- `<leader>as`: Apply code action affecting the whole file.
- `<leader>cl`: Run the code lens action on the current line
- `<leader>o`: Organize imports.
- `gd`: Goto definition of current hovered word.
- `gy`: Goto type definition of current hovered word.
- `gi`: Goto implementation of current hovered word.
- `gi`: Goto implementation of current hovered word.
- `gr`: Show and goto references of current hovered word.
- `<leader>k` or `K`: Show documentation of current hovered word.
- `<Ctrl-w>w`: (Un-)Fullscreen current window.
- `<Ctrl-p>`: Open a file with fuzzy search and preview.
- `<Ctrl-n>`: Toggle Nerdtree.
- `<Ctrl-b>` or `Ctrl-PageUp`: Scroll up in floating window.
- `<Ctrl-f>` or `Ctrl-PageDown`: Scroll down in floating window.

### Insert mode
- `<TAB>`: Select next autocomplete option.
- `<Shift-TAB>`: Select prev autocomplete option.
- `<Enter>`: Select autocomplete option.
- `<Ctrl-Space>`: Trigger autocompletion.

### Visual mode
- `<leader>f`: Reformat selected line.
- `<leader>a`: Apply code action to the selected block.


## How-To-Uninstall

```bash
~/.vim/fzf/uninstall
rm -rvf ~/.vim ~/.config/coc/
```

## Vim Version

```
$ vim --version
VIM - Vi IMproved 8.2 (2019 Dec 12, compiled Jan 21 2021 13:13:41)
Included patches: 1-814
Modified by Gentoo-8.2.0814-r100
Compiled by jake@localhost
Huge version without GUI.  Features included (+) or not (-):
+acl               -farsi             +mouse_sgr         +tag_binary
+arabic            +file_in_path      -mouse_sysmouse    -tag_old_static
+autocmd           +find_in_path      +mouse_urxvt       -tag_any_white
+autochdir         +float             +mouse_xterm       -tcl
-autoservername    +folding           +multi_byte        +termguicolors
-balloon_eval      -footer            +multi_lang        -terminal
+balloon_eval_term +fork()            -mzscheme          +terminfo
-browse            +gettext           +netbeans_intg     +termresponse
++builtin_terms    -hangul_input      +num64             +textobjects
+byte_offset       +iconv             +packages          +textprop
+channel           +insert_expand     +path_extra        +timers
+cindent           +ipv6              -perl              +title
+clientserver      +job               +persistent_undo   -toolbar
+clipboard         +jumplist          +popupwin          +user_commands
+cmdline_compl     +keymap            +postscript        +vartabs
+cmdline_hist      +lambda            +printer           +vertsplit
+cmdline_info      +langmap           +profile           +virtualedit
+comments          +libcall           -python            +visual
+conceal           +linebreak         +python3           +visualextra
+cryptv            +lispindent        +quickfix          +viminfo
-cscope            +listcmds          +reltime           +vreplace
+cursorbind        +localmap          +rightleft         +wildignore
+cursorshape       -lua               -ruby              +wildmenu
+dialog_con        +menu              +scrollbind        +windows
+diff              +mksession         +signs             +writebackup
+digraphs          +modify_fname      +smartindent       +X11
-dnd               +mouse             +sound             +xfontset
-ebcdic            -mouseshape        +spell             -xim
+emacs_tags        +mouse_dec         +startuptime       -xpm
+eval              +mouse_gpm         +statusline        +xsmp_interact
+ex_extra          -mouse_jsbterm     -sun_workshop      +xterm_clipboard
+extra_search      +mouse_netterm     +syntax            -xterm_save
   system vimrc file: "/etc/vim/vimrc"
     user vimrc file: "$HOME/.vimrc"
 2nd user vimrc file: "~/.vim/vimrc"
      user exrc file: "$HOME/.exrc"
       defaults file: "$VIMRUNTIME/defaults.vim"
  fall-back for $VIM: "/usr/share/vim"
Compilation: x86_64-pc-linux-gnu-gcc -c -I. -Iproto -DHAVE_CONFIG_H     -march=native -O2 -pipe -D_REENTRANT  -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=1       
Linking: x86_64-pc-linux-gnu-gcc   -Wl,-O1 -L/usr/local/lib -Wl,--as-needed -o vim    -lSM -lICE -lXpm -lXt -lX11 -lXdmcp -lSM -lICE  -lm -ltinfo -lelf   -lcanberra  -lacl -lattr -lgpm -ldl     -lpython3.8 -lcrypt -lpthread -ldl -lutil -lm -lm      
```
