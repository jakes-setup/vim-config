#!/bin/bash

# list files for fzf

function list_all_files() {
	if git branch >/dev/null ; then
		# inside git repository
		PREFIX="$(git rev-parse --show-toplevel)"
		pushd "$PREFIX" >/dev/null
		ALLFILES="$( ( git status --short| grep '^?' | cut -d\  -f2- && git ls-files ) | sort -u)"
		popd >/dev/null
		echo "$ALLFILES" | while read line ; do
			line="${PREFIX}/$line"
			if [ -d "$line" ] && ( ! ( git ls-files --error-unmatch "$line" >/dev/null 2>/dev/null ) ) ; then
				# Found untracked directory
				#echo "DEBUG: $line"
				find "$line" -type f | while read line2 ; do
					realpath -s --relative-to=. "${line2}"
				done
			elif [ ! -d "$line" ]; then
				# Normal file
				realpath -s --relative-to=. "$line"
			fi
		done | LANG=C sort -u --reverse
	else
		# not inside git repository
		find .
	fi
}

function filter_remove_binaries() {
	cat - | while read line; do
		mt="$(file -b -i -L -- "${line}")"
		# application/octet-stream; charset=binary
		#if echo "x${mt}y" | grep -q "^xapplication.octet.stream.*binary.*y$" ; then
		if echo "x${mt}y" | grep -q "binary" && [ -s "${line}" ]; then
			true
			#echo "(binary file) ${line}"
		else
			echo "$line"
		fi
		#echo "yeet: ${mt}: $line"
		#echo "$line"
	done

}

list_all_files | filter_remove_binaries
