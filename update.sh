#!/bin/bash

set -e

pushd ~/.vim

# Update repo
git submodule init
git submodule update
git pull --recurse-submodules


## Install lsp & coc.nvim requirements
mkdir -p node
curl -sfLS install-node.vercel.app/lts | bash -s -- --prefix=./node --force
export PATH="${HOME}/.vim/node/bin:$PATH"

# Install yarn
corepack prepare yarn@stable --activate
#yarn set version stable
ln -f -s ../lib/node_modules/corepack/dist/yarn.js ./node/bin/yarn
ln -f -s ../lib/node_modules/corepack/dist/yarn.js ./node/bin/yarn.js
ln -f -s ../lib/node_modules/corepack/dist/yarnpkg.js ./node/bin/yarnpkg
ln -f -s ../lib/node_modules/corepack/dist/yarnpkg.js ./node/bin/yarnpkg.js
yarn config set --home enableTelemetry 0

#### Install texlab server for latex lsp
#mkdir -p texlab
#pushd texlab
#if [ ! -f v550 ]; then
#	wget https://github.com/latex-lsp/texlab/releases/download/v5.5.0/texlab-x86_64-linux.tar.gz -O texlab.tar.gz
#	tar xvzf texlab.tar.gz
#	rm -f texlab.tar.gz
#	touch v550
#fi
#popd


#curl --compressed -o- -L https://yarnpkg.com/install.sh | HOME=. bash

# Install fzf
pushd ~
~/.vim/fzf/install --all --no-update-rc --no-zsh --no-fish --completion
popd



# Install Plugins
function vimexecsleep() {
	vim -c ":${1} | echo \"Sleeping ${2} seconds...\" | sleep ${2} | qall"
}

vimexecsleep ":PlugInstall" 3
vimexecsleep ":PlugUpgrade" 3
vimexecsleep ":PlugUpdate" 6
vimexecsleep ":PlugInstall" 3
#mkdir -p ~/.vim/doc
#vimexecsleep ":helptags ~/.vim/doc" 2


# Install Coc extensions
#vimexecsleep ":CocInstall coc-tsserver coc-json coc-html coc-css coc-pyright coc-snippets coc-prettier coc-ansible coc-clangd coc-cmake coc-git coc-highlight coc-java coc-sh coc-svg coc-yaml coc-xml coc-zig" 3
#vim -c "CocInstall coc-tsserver coc-json coc-html coc-css coc-pyright coc-snippets coc-prettier coc-clangd coc-cmake coc-git coc-highlight coc-java coc-sh coc-svg coc-yaml coc-xml coc-zig|q"
#vimexecsleep ":CocUpdateSync" 3
mkdir -p ~/.config/coc/extensions
pushd ~/.config/coc/extensions
if [ ! -f package.json ]
then
  echo '{"dependencies":{}}'> package.json
fi
# Change extension names to the extensions you need
function installcocext() {
	echo "Installing extension: $1"
	#~/.vim/node/bin/npm install "${1}" --global-style --ignore-scripts --no-bin-links --no-package-lock --only=prod
	~/.vim/node/bin/npm install "${1}" --install-strategy=shallow --ignore-scripts --no-bin-links --no-package-lock --only=prod
}

installcocext coc-tsserver
installcocext coc-json
installcocext coc-html
installcocext coc-css
#installcocext coc-pyright
#installcocext coc-pylsp
#installcocext coc-jedi
installcocext coc-snippets
installcocext coc-prettier
#installcocext coc-ansible
installcocext coc-clangd
installcocext coc-cmake
installcocext coc-git
installcocext coc-highlight
installcocext coc-java
#installcocext coc-sh
installcocext coc-svg
installcocext coc-yaml
installcocext coc-xml
installcocext coc-zig
#installcocext coc-vimtex
installcocext coc-texlab

#pip3 install -U jedi-language-server

echo "Installing python-lsp-server"
pip3 install python-lsp-server
pip3 install python-lsp-server[all]
#vim -c "CocInstall @yaegassy/coc-pylsp|q"

popd

# Install bash-language-server
mkdir -p bash-language-server
pushd bash-language-server
	#~/.vim/node/bin/npm install bash-language-server --install-strategy=shallow --ignore-scripts --no-bin-links --no-package-lock --only=prod
	~/.vim/node/bin/npm install bash-language-server --ignore-scripts --no-bin-links --no-package-lock --only=prod
popd

# Install clangd
#vim -c ":CocCommand clangd.install | :CocCommand clangd.update | qall!" test.c # TODO nur auskommentiert weil :q nicht funktioniert.

popd
